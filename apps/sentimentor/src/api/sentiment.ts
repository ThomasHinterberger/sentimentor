import axios, { AxiosResponse } from "axios";

const axiosSentiment = axios.create({
    baseURL: "http://127.0.0.1:8000/api/v1"
})

export const serverTest = async (): Promise<boolean> => {
    const res = await axiosSentiment.get("")
        .then((res: AxiosResponse<boolean>) => res);
    return res.data;
}

export interface ISentimentResponse {
    sentiment: number
    probability: number
}

export const textToSentiment = async (text: string, model: string): Promise<ISentimentResponse> => {
    const res = await axiosSentiment.post("sentiment", null, { params: { text, model } })
    return res.data;
}

export const textsToSentiment = async (texts: Array<string>, model: string) => {
    const res = await axiosSentiment({
        method: "post",
        url: "/sentiments",
        params: { model },
        data: { texts }
    })
    return res.data;
}