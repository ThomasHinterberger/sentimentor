from asyncio.windows_events import NULL
from multiprocessing.dummy import Array
from tkinter.tix import INTEGER
from tokenize import String
from typing import Optional
from fastapi import APIRouter,  FastAPI
from pydantic import BaseModel
from fastapi.middleware.cors import CORSMiddleware
from pydantic import BaseModel
import uvicorn
from load_models import load_t_model
from transformers import pipeline


prefix_router = APIRouter()

t_model = load_t_model()
transformers_model = pipeline("text-classification","finiteautomata/bertweet-base-sentiment-analysis")

    

@prefix_router.get("/")
def read_root():
    return True

def convert_sentiment(x:str):
    if x == "NEG":
        return -1
    elif x == "NEU":
        return 0
    else:
        return 1

@prefix_router.post("/sentiment")
def sentiment(text:str, model:str):
    if model == "sentimentor":
        prediction = t_model.predict(text)
        return {
        "sentiment":int(float(prediction[0])),
        "probability":float(prediction[2][(int(float(prediction[0]))+1)])
        }
    elif model == "transformers":
        prediction = transformers_model(text)
        return {
            "sentiment": convert_sentiment(prediction[0]["label"]),
            "probability": prediction[0]["score"]
        }
    else:
        return None    

class Item(BaseModel):
    texts:list[str]


@prefix_router.post("/sentiments")
async def sentiments(body: Item, model:str):
    if model == "transformers":
        return transformers_model(body.texts)
    elif model == "sentimentor":
        sents = []
        for i in body.texts:
            print(i)
            prediction = t_model.predict(i)
            sents.append({
            "label":int(float(prediction[0])),
            "score":float(prediction[2][(int(float(prediction[0]))+1)])
            })
        return sents


app = FastAPI()

origins = [
    "*",
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

app.include_router(
    prefix_router,
    prefix="/api/v1"
)

if __name__ == "__main__":
    uvicorn.run(app, host="127.0.0.1", port=8000)